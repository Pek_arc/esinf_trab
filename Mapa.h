#ifndef _Mapa_
#define _Mapa_

#include <string>
#include <unordered_map>
#include "graphStlPath.h"
#include "PVia.h"
#include "Teste.h"


class Mapa :  public graphStlPath <Local*, PVia>
{
private:
	stack <pair<Local *, PVia>> menorCaminho( Local* const &ci,  Local* const &cf);
public:
	Mapa();
	Mapa(const Mapa &m);


	void caminhosPossiveis(Local* const &ci,  Local* const &cf);
	void menorCaminhoDistancia(Local* const &ci,  Local* const &cf);
	void menorCaminhoCusto(Local* const &ci,  Local* const &cf);
	stack <pair<Local *, PVia>> menorCaminhoTempo(Local* const &ci,  Local* const &cf);
	void maiorInteresseTuristico(Local* const &ci,  Local* const &cf);
	void imprimeItenerario(stack <pair<Local *, PVia>> &it);
	bool lerFicheiroItenerario(string itn, queue<Local *> &itener);
	void calculaItenerario(string f);
	string minToTime(int min) const;
};

Mapa::Mapa() : graphStlPath <Local*, PVia>() {
}
Mapa::Mapa(const Mapa &m) : graphStlPath <Local*, PVia>(m) {
}

//Converte o tempo em minutos no ormato HH:MM
string Mapa::minToTime(int min) const {
	stringstream hora;
	hora << min / 60 << "h:"<<min % 60<< "m";

	return hora.str();
}

//Lista todos os caminhos possiveis entre ci(Local inicial) e cf (Local Final)
void Mapa::caminhosPossiveis(Local* const &ci,  Local* const &cf){
	cout << "Caminhos Diferentes entre: " << ci->getDesc() <<" e " << cf->getDesc() << endl;
	queue < stack <pair<Local*, PVia>>>  q2; // A cada Vertice do caminho est� associado a Via utilizada para o Vertice seguinte
	q2 = distinctPaths(ci,cf);
			stack <pair<Local*, PVia>> aux;
	while(!q2.empty()){ 
		while(!q2.front().empty()){ //Inverte a Stack para ser imprimida corretamente
			aux.push(q2.front().top());
			q2.front().pop();
		}
		imprimeItenerario(aux);
		cout << endl;
		q2.pop();
	}
	cout << endl;
}

//Calcula o menor caminho entre ci e cf, utilizando o valor da distancia da via como comparador das Vias
void Mapa::menorCaminhoDistancia(Local* const &ci, Local* const &cf) {

	PVia::setComparacaoKMS();
	cout << "Menor distancia entre: " << ci->getDesc() <<" e " << cf->getDesc() << endl;
	stack <pair<Local *, PVia>> it;
	it=menorCaminho(ci, cf);
	imprimeItenerario(it);
}

//Calcula o menor caminho entre ci e cf, utilizando o custo, em euros, da via como comparador das Vias
void Mapa::menorCaminhoCusto(Local* const &ci, Local* const &cf) {

	PVia::setComparacaoCUSTO();
	cout << "Menor custo entre: " << ci->getDesc() <<" e " << cf->getDesc() << endl;
	stack <pair<Local *, PVia>> it;
	it=menorCaminho(ci, cf);
	imprimeItenerario(it);
}

//Calcula o menor caminho entre ci e cf, utilizando o tempo m�dio da Via como comparador das Vias
stack <pair<Local *, PVia>> Mapa::menorCaminhoTempo(Local* const &ci, Local* const &cf) {
	PVia::setComparacaoTEMPO();
	cout << "Menor tempo entre: " << ci->getDesc() <<" e " << cf->getDesc() << endl;
	stack <pair<Local *, PVia>> it;
	it=menorCaminho(ci, cf);
	return it;

}

//Devolve uma Stack com o caminho com menor custo (Kms, Euros ou Tempo) entre ci e cf
stack <pair<Local *, PVia>> Mapa::menorCaminho(Local* const &ci, Local* const &cf) {
		vector <pair<int, PVia>> pathKeys; //Alem de um valor inteiro com a Key do Local que o antecede, ret�m tamb�m a Via utilizada para o alcan�ar
		vector <PVia> dist;
		int kDest;
		stack <pair<Local *, PVia>> q3;
		if(vlist.empty()){
			cout << "Grafo Vazio" << endl;
			return q3;
		}
		dijkstrasAlgorithm(ci, pathKeys, dist);
		getVertexKeyByContent(kDest, cf);
		q3=getDijkstrasPath(kDest,pathKeys);
		return q3;
	}

//Metodo para Imprimir todos os caminhos dos m�todos anteriores
void Mapa::imprimeItenerario(stack <pair<Local *, PVia>> &it) {
	while(!it.empty()){
		cout << it.top().first->getDesc();
		if (it.top().second.getDesc().size()) cout << "-<" <<it.top().second.getDesc() << ">-"; //Por cada Local, imprime tamb�m a Via utilizada (stack <pair<Local *, PVia>>)
		it.pop();
	}
	cout << endl;
}

void Mapa::maiorInteresseTuristico(Local* const &ci,  Local* const &cf){
	cout << "Caminho mais interessante entre : " << ci->getDesc() <<" e " << cf->getDesc() << endl;
	queue < stack < pair <Local*, PVia>>>  q3 = distinctPaths(ci,cf);
	int max =0;
	stack < pair <Local*, PVia>> maior;
	while(!q3.empty()){ //Encontra o caminho com mais Locais
		if(((int)q3.front().size())> max){
			max = ((int)q3.front().size());
			q3.front().swap(maior);
		}
		q3.pop();
	}
	stack <pair<Local*, PVia>> aux;
	while(!maior.empty()){ //Inverte a ordem da stack, para imprimir correctamente
		aux.push(maior.top());
		maior.pop();
	}
	imprimeItenerario(aux);
	cout << endl;
}

/*
Semelhante ao metodo da classe teste, mas que valida a existencia dos locais no Grafo da classe Mapa
Apenas l� do ficheiro a descri��o do Local
*/
bool Mapa::lerFicheiroItenerario(string itn, queue<Local *> &itener){
	string linha ;
	int inic = 0;
	ifstream fx ;
	string argL[4];
	fx.open(itn) ;
	if (!fx)
	{ cout << "Fx. nao existe !" << endl ;
	return false ;
	}
	while (!fx.eof()){
		int arg = 0;
		int pos = 0;
		getline(fx,linha,'\n');
		if (linha.size() > 0)
		{
			int pos = linha.find(',', inic);
			while (pos > 0){
				pos = linha.find(',', inic);
				argL[arg] = (linha.substr(inic,pos-inic));
				pos++ ;
				inic = pos ;
				arg++;
			}
			Local* l=new Local();
			Local* lf = new Local(argL[0]);
			if (!getVertexContentBySubContent(l, lf)){
				cout << "Local nao existe, Via: "<<endl;
			}else {
				itener.push(l);
			}
		}
	}
	fx.close() ;
	return true;
} 

void Mapa::calculaItenerario(string f) {
	if(vlist.empty()){
		cout << "Grafo Vazio" << endl;
		return;
	}
	queue <Local *> itener;
	bool error=lerFicheiroItenerario(f, itener);
	if(!error) return;
	stack <pair<Local *, PVia>> it;
	int tp; //tempo parcial entre 2 locais
	int tp24; //tempo parcial durante a visita no intervalo de 24h
	Local * li=itener.front();
	itener.pop();
	PVia via;
	int inicioVisita;
	cout << "Indique o inicio da visita: (Minutos apos 00:00)" << endl;
	cin >> inicioVisita;
	while(inicioVisita<0 || inicioVisita>1439){
		cin.clear();
		cin.ignore(numeric_limits<int>::max(), '\n');
		system ("cls");
		cout << "Valor invalido, insira novamente" << endl;
		cout <<  "Indique o inicio da visita: (Minutos apos 00:00)" << endl;
		cin >> inicioVisita;
	}
	cout << "Inicia da Visita:  " << minToTime(inicioVisita) << endl << endl;
	int fimVisita =inicioVisita;
	int fimVisita24 = fimVisita % 1440; //Caso a visita se prolongue por mais que um dia, guarda os minutos do dia, para verificar as condi�oes de visita dos locais historicos

	while(!itener.empty()){
		bool visita= true;
		it=menorCaminhoTempo(li, itener.front());
		tp=0; //tempo parcial entre cada local
		while(!it.empty()){ //soma o tempo do itenerario entre dois locais
			tp +=it.top().second.getTempo();
			it.pop();
		}
		if(tp>0){ // se o itenerario devolve 0 min, significa que nao existe liga��o entre os dois locais
			cout << "Tempo da vigem:  " <<minToTime(tp)<< endl;
			tp24 = (fimVisita24+tp) % 1440; //Tempo de Chegada ao local no formato 24h
			cout << "Chegada ao local:  " << itener.front()->getDesc() << " as: "<<minToTime(tp24)<< endl;
			if(typeid(*itener.front())==typeid(Localh)){
				cout << "Local Historico"<< endl;
				cout << "Abertura:  " <<minToTime((itener.front()->getAbertura())) << endl;
				cout << "Fecho:  " <<minToTime((itener.front()->getFecho())) << endl;
				if(tp24<(itener.front()->getAbertura())){ //Antes do tempo de abertura - espera
					cout << "Espera: " << minToTime((itener.front()->getAbertura())-tp24) << "min" << endl;
					tp+=(itener.front()->getAbertura())-tp24;
					cout << "Inicio da Visita: " <<minToTime((itener.front()->getAbertura())) << "  Duracao: "<< minToTime((itener.front()->getTempo())) <<endl;
				}else{
					if(tp24>(itener.front()->getFecho())){ //Depois do tempo de Abertura- espera pelo dia seguite
						cout << "Espera: " << minToTime(1440-(tp24-(itener.front()->getAbertura()))) << "min" << endl;
						tp+=1440-(tp24-(itener.front()->getAbertura()));
						cout << "Inicio da Visita: " << minToTime((itener.front()->getAbertura())) << "  Duracao: "<< minToTime((itener.front()->getTempo())) <<endl;
					}else{
						if(tp24+itener.front()->getTempo()>itener.front()->getFecho()){ //O tempo de chegada nao permite visitar o local - descarta o local
							visita=false;
							cout << "Local nao Visitado, nao existe tempo suficiente:  " <<(itener.front()->getDesc()) << endl;
						}
					}
				}
			}else cout << "Local Natural"<< endl; 
		}else{
			cout << "Nao existe liga��o" << endl;
			visita=false;
		}
		if(visita) { //Se efetua a visita, contabiliza os tempos envolvidos
			fimVisita+=tp+itener.front()->getTempo();
			li=itener.front();
			itener.pop();
		}else {
			itener.pop(); //se nao visita, descarta o local e tenta alcan�ar o proximo, considerando que esse trajecto nao foi efectuado
		}
		cout << "Tempo Parcial da Visita: " << minToTime(fimVisita) << endl; //Duracao actual da visita
		fimVisita24 = fimVisita % 1440;

	}
	cout << endl << "Fim da Visita:  " << minToTime(fimVisita%1440) << endl; //Fim da visita no formata 24h
	cout << endl << "Fim da Visita, em minutos absolutos:  " << fimVisita << endl; //Fim da visita contada em minutos desde as 00:00 do dia de partida
	cout << endl << "Duracao da Visita:  " << minToTime(fimVisita-inicioVisita) << endl; //Duracao total da visita

}

#endif