#include <iostream>
#include <string>
#include <list>
#include "Localn.h"
#include "Localh.h"
#include "Via.h"
#include "Local.h"
#include "EN.h"
#include "AE.h"
#include "Teste.h"
#include "Mapa.h"
#include "PVia.h"


using namespace std;

int main(){

Teste t;
Mapa m;
string loc1;
string loc2;
Local * lInicial= new Local ();
Local * lFinal= new Local ();
PVia via= PVia();
int opcao=0;
do{
cout<<"****************************************"<<endl;
cout<<"**           ESINF  - 13-14           **"<<endl;
cout<<"****************************************"<<endl;
cout<<"                                        "<<endl;
cout<<"Escolha uma das seguintes op�oes"<<endl;
cout<<"1   -  Ler Ficheiro de Locais"<<endl;
cout<<"2   -  Ler Ficheiro de Vias"<<endl;
cout<<"3   -  Listar Locais"<<endl;
cout<<"4   -  Ordenar Locais Alfabeticamente"<<endl;
cout<<"5   -  Listar Vias"<<endl;
cout<<"6   -  Contabilizar Locais"<<endl;
cout<<"7   -  Construir o Grafo"<<endl;
cout<<"8   -  Imprimir o Grafo"<<endl;
cout<<"9   -  Caminhos entre dois Locais"<<endl;
cout<<"10  -  Caminho mais curto entre dois Locais"<<endl;
cout<<"11  -  Caminho mais economico entre dois Locais"<<endl;
cout<<"12  -  Caminho mais interessante entre dois Locais"<<endl;
cout<<"13  -  Mostrar Itenerario escolhido"<<endl;
cout<<"0   -  Terminar"<<endl;


cin>>opcao;
switch (opcao){
case 1:
		t.lerFicheiroLocais("locais.txt");
		system ("pause");
		break;
case 2:
		t.lerFicheiroVias("ligacoes.txt");
		system ("pause");
		break;
case 3:
		t.listarLocais();
		system ("pause");
		break;
case 4:
		t.ordenarLocais();
		system ("pause");
		break;

case 5:
		t.listarVias();
		system ("pause");
		break;

case 6:
		t.ordenarLocais();
		t.contabLocais();
		system ("pause");
		break;

case 7:
		t.constroiGrafo(m);
		system ("pause");
		break;

case 8:
		cout << m << endl;
		system ("pause");
		break;

case 9:
		cout << "Insira o Local inicial: " << endl;
		cin >> loc1;
		cout << "Insira o Local de Destino: " << endl;
		cin >> loc2;
		if((t.find(lInicial, loc1)) && (t.find(lFinal, loc2))){
		m.caminhosPossiveis(lInicial, lFinal);
		}else cout << "Um dos Locais nao existe" << endl;
		system ("pause");
		break;


case 10:
		cout << "Insira o Local inicial: " << endl;
		cin >> loc1;
		cout << "Insira o Local de Destino: " << endl;
		cin >> loc2;
		if((t.find(lInicial, loc1)) && (t.find(lFinal, loc2))){
		m.menorCaminhoDistancia(lInicial, lFinal);
		}else cout << "Um dos Locais nao existe" << endl;
		system ("pause");
		break;

case 11:
		cout << "Insira o Local inicial: " << endl;
		cin >> loc1;
		cout << "Insira o Local de Destino: " << endl;
		cin >> loc2;
		if((t.find(lInicial, loc1)) && (t.find(lFinal, loc2))){
		m.menorCaminhoCusto(lInicial, lFinal);
		}else cout << "Um dos Locais nao existe" << endl;
		system ("pause");
		break;

case 12:
		cout << "Insira o Local inicial: " << endl;
		cin >> loc1;
		cout << "Insira o Local de Destino: " << endl;
		cin >> loc2;
		if((t.find(lInicial, loc1)) && (t.find(lFinal, loc2))){
		m.maiorInteresseTuristico(lInicial, lFinal);
		}else cout << "Um dos Locais nao existe" << endl;
		system ("pause");
		break;

case 13:
		m.calculaItenerario("Itenerario.txt");
		system ("pause");
		break;
		
case 0:
	return 0;

default:
cout<<"Op�ao n�o v�lida"<<endl;
		system ("pause");
}
system ("cls");
}while (opcao !=0);
return 0;
};

