#ifndef Utils_
#define Utils_

#include <string>
#include <sstream>
using namespace std;


class Utils{

private:


public:
Utils();
string minToTime(int min) const;

};

Utils::Utils(){
}

string Utils::minToTime(int min) const {
	stringstream ss;
	ss << min / 60 << "h:"<<min % 60<< "m";

	return ss.str();
}

#endif