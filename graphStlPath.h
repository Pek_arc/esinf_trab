/***************************************
 * graphStl.h
 *
 * Created on 19 de Julho de 2013, 11:51
 *
 * @author Nuno Malheiro
 * rev1: g++ compatibility 23/10/2013
 * rev2: mac compatibility 29/10/2013
 ***************************************/

#ifndef _graphStlPath_
#define _graphStlPath_

#include <string.h> // memset include for g++
#include <stack>
#include <queue>
#include <type_traits>
#include <bitset>
#include <vector>
#include <list>
#include <vector>


using namespace std;

#include "graphStl.h"

/*
Esta classe baseia-se na classe desenvolvida nas aulas, alterando-se apenas os metodos para utilizarem o tipo de dados pair<int, TE> e pair<TV, TE>
Solu��o adoptada para mostrar, nos caminhos, alem do conteudo dos Vertices (Local*), tamb�m o conteudo dos Ramos (Pvia (Via*))
*/

template<class TV,class TE>
class graphStlPath : public graphStl <TV,TE>
{
protected:	
	void distinctPathsRecursive(typename list < graphVertex <TV,TE> >::iterator &itvo,typename list < graphVertex <TV,TE> >::iterator &itvd, bitset <MAX_VERTICES> &taken, stack < pair <TV, TE>> &s,  queue < stack < pair <TV, TE>>> &qr);

public:
	graphStlPath();
	queue < stack <pair<TV, TE>>> distinctPaths(const TV &vOrigin,const TV &vDestination);	
	bool dijkstrasAlgorithm(const TV &vContent, vector <pair<int, TE>> &pathKeys, vector <TE> &dist);
	stack <pair<TV, TE>> getDijkstrasPath(int vKeyOrigin, vector <pair<int, TE>> pathKeys);
};

template<class TV,class TE>
graphStlPath<TV,TE>::graphStlPath() : graphStl<TV,TE>()
{
}


template<class TV,class TE>
queue < stack <pair<TV, TE>>> graphStlPath <TV,TE>::distinctPaths(const TV &vO,const TV &vD){
	queue < stack <pair<TV, TE>>> qResult;
	stack <pair<TV, TE>> s;
	typename list <graphVertex<TV,TE>>::iterator itvorigem, itvdestino;
	if(!getVertexIteratorByContent(itvorigem,vO) || !getVertexIteratorByContent(itvdestino,vD) ){
		cout << "Erro, vertice nao encontrado";
		return qResult;
	}
	bitset <MAX_VERTICES> taken(0);
	distinctPathsRecursive(itvorigem, itvdestino, taken, s, qResult);
	return qResult;
}


template<class TV,class TE>
void graphStlPath <TV,TE>::distinctPathsRecursive(typename list < graphVertex <TV,TE> >::iterator &itvo, typename list < graphVertex <TV,TE> >::iterator &itvd, bitset <MAX_VERTICES> &taken, stack < pair <TV, TE>> &s,  queue < stack < pair <TV, TE>>> &qr){
	taken[itvo->getVKey()]=1;
	s.push(pair<TV,TE>(itvo->getVContent(),TE())); 
	for(typename list<graphEdge<TV,TE>>::iterator ite = itvo->getAdjacenciesBegin(); ite!=itvo->getAdjacenciesEnd();ite++){
		s.top().second=ite->getEContent();
		if(ite->getVDestination()->getVKey() == itvd->getVKey()){
			s.push(pair<TV,TE>(ite->getVDestination()->getVContent(), TE()));
			qr.push(s);
			s.pop();
		}else {
			if(taken[ite->getVDestination()->getVKey()] == 0)
				distinctPathsRecursive(ite->getVDestination(), itvd, taken, s, qr);
		}
	}
	taken[itvo->getVKey()] = 0;
	s.pop();
}

template<class TV,class TE>
bool graphStlPath <TV,TE>::dijkstrasAlgorithm(TV const &vContent, vector <pair<int, TE>> &pathKeys, vector <TE> &dist){

	vector <typename list <graphVertex <TV,TE>>::iterator> path;

	bitset <MAX_VERTICES> process;
	pathKeys.clear(); dist.clear();

	for (typename list <graphVertex<TV,TE> >::iterator it=vlist.begin();it!=vlist.end(); it++){
		path.push_back(it);
		pathKeys.push_back(pair<int,TE>(-1, TE()));
		dist.push_back(getInfinite());
	}

	typename list <graphVertex<TV,TE>>::iterator itv;
	if(!(getVertexIteratorByContent(itv,vContent))) return false;
	TE zeroValue;
	if((is_integral<TE>::value) || (is_floating_point<TE>::value)) memset(&zeroValue, 0, sizeof(zeroValue));

	dist[itv->getVKey()] = zeroValue;

	while(itv!=vlist.end()){
		process[itv->getVKey()]=1;
		for(typename list <graphEdge <TV,TE> >::iterator ite =itv->getAdjacenciesBegin(); ite!=itv->getAdjacenciesEnd(); ite++){
			int destKey = ite->getVDestination()->getVKey();
			if((!process[destKey]) && dist[destKey] > dist[itv->getVKey()] + ite->getEContent()) {
				path[destKey]=itv;
				pathKeys[destKey]=(pair<int,TE>(itv->getVKey(), ite->getEContent()));
				dist[destKey]=dist[itv->getVKey()] + ite->getEContent();
			}
		}
		TE min = getInfinite();
		int vmin = -1;
		for(size_t i=0;i<dist.size();i++){

			if(!process[i] && dist[i] < min){
				min= dist[i];
				vmin = i;

			}
		}
	if(vmin == -1) itv=vlist.end(); else getVertexIteratorByKey(itv, vmin);
	}
	return true;
}


template<class TV,class TE>
stack <pair<TV, TE>> graphStlPath <TV,TE>::getDijkstrasPath(int vKeyDestiny, vector <pair<int, TE>> pathKeys){
	stack <pair<TV, TE>> path;
	if(pathKeys[vKeyDestiny].first!=-1){
		TV dest;
		TE via= TE();
		getVertexContentByKey(dest, vKeyDestiny);
		int vi = vKeyDestiny;
		while(vi!=-1){
			getVertexContentByKey(dest, vi);
			path.push(pair<TV, TE>(dest, via));
			via=pathKeys[vi].second;
			vi=pathKeys[vi].first;
		}
	}

	return path;
}


#endif