#ifndef Local_
#define Local_

#include <string>
#include <sstream>
using namespace std;


class Local
{
	string desc;

public:
	Local();
	Local(string d);
	Local(const Local &p);
	~Local();

	bool operator < (const Local& l);
	virtual bool operator == (const Local &l);
	virtual const Local& operator = (const Local &l);


	virtual void escreve (ostream& out) const;
	virtual int getTempo() const;
	virtual int getFecho()const;
	virtual int getAbertura()const;

	string getDesc()const;
	void setDesc(string d);

	string minToTime(int min) const;
	virtual Local* clone() const;

};

Local::Local()
{
desc="";
}

Local::Local(string d){
	desc=d;
}

Local::Local(const Local &t){
	desc=t.desc;
}

Local::~Local(){
}



string Local::getDesc() const{
	return desc;
}

int Local::getTempo() const{
	return 0;
}

int Local::getFecho() const{
	return 0;
}

int Local::getAbertura() const{
	return 0;
}

void Local::setDesc(string d){
	desc=d;
}


bool Local::operator ==(const Local &l){
		if (desc == l.desc){
		return true;
	}else return false;
}


bool Local::operator < (const Local& l){

    return (desc < l.desc);
}


const Local& Local:: operator = (const Local &l){
	(*this).desc = l.desc;
	return *this;

}



void Local::escreve(ostream &out) const{
	out << "Descricao: " << desc << endl;
}

ostream& operator << (ostream& out, const Local& l){
	l.escreve(out);
	return out;
}

string Local::minToTime(int min) const {
	stringstream ss;
	ss << min / 60 << "h:"<<min % 60<< "m";

	return ss.str();
}

Local * Local :: clone() const
{
	return new Local(*this);
}

#endif