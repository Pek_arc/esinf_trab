#ifndef Localn_
#define Localn_

#include <string>
using namespace std;

#include "Local.h"

class Localn : public Local{

	float area;


public:
	Localn();
	Localn(string d, float a);
	Localn(const Localn &l);
	virtual ~Localn();

	float getArea();


	void setArea(float a);
	virtual int getTempo() const;
	virtual int getAbertura() const;
	virtual int getFecho() const;


	const Localn& operator = (const Localn &l);
	virtual bool operator==(const Localn &c);
	virtual void escreve (ostream& out) const;
	virtual Local * clone() const;

};



Localn::Localn(){
	}

Localn::Localn(string d, float a):Local(d){
	area=a;
}

Localn::Localn(const Localn &l):Local(l){

	area=l.area;
}



Localn::~Localn(){
}


float Localn::getArea(){
	return area;
}


void Localn::setArea(float a){
	area=a;
}

int Localn::getTempo() const{
	return 0;
}

int Localn::getAbertura()const{
	return 0;
}

int Localn::getFecho()const{
	return 1440;
}

const Localn &Localn:: operator = (const Localn &l){
		Local::operator=(l);
		area = l.area;

	return *this;

}

bool Localn::operator==(const Localn &l){
		if ((*this).getDesc() == l.getDesc() && area == l.area){
		return true;
	}else return false;
}

void Localn::escreve(ostream &out) const{
	Local::escreve(out);
	out << "Area: " << area << endl;
}

ostream& operator << (ostream& out, const Localn& p){
	p.escreve(out);
	return out;
}

Local * Localn::clone() const
{
	return new Localn (* this);
}

#endif