#pragma once;
#include <iostream>
#include <string>
#include "Via.h"
#include "Local.h"
using namespace std;

class AE: public Via{

private:
	double preco;


public:
	AE();
	AE( Local* &li, Local* &lf,  string d, int c, int t ,double p);
	AE(string d, int c, int t ,double p);
	AE(const AE&);
	~AE();

	virtual double getPreco() const;
	void setPreco(double);

	virtual Via * clone();

	const AE& operator = (const AE &v);
	virtual void escreve(ostream& out)const;


};// fim classe -----------------------------

AE::AE():Via(){

	preco=0.0;
}

AE::AE(Local* &li, Local* &lf, string d, int c, int t ,double p): Via(li, lf, d,c,t){
		(*this).setPreco(p);
}

AE::AE(string d, int c, int t ,double p): Via(d,c,t){
		(*this).setPreco(p);
}

AE::AE(const AE& v):Via(v){
	(*this).setPreco(v.preco);
}


AE::~AE(){}

double AE::getPreco() const{

	return (*this).preco;

}

void AE::setPreco(double p){

	(*this).preco=p;
}


void AE::escreve(ostream& out)const{

	Via::escreve(out);
	out<<" Preco: "<<preco<<" Euros"<<endl;
}

const AE& AE:: operator = (const AE& n){
		if(&n != this){
		(*this)=AE(n);
	}
	return *this;

}

Via * AE::clone(){

	return new AE(*this);
}


//------------OSTREAM

ostream& operator<<(ostream& out, AE& x){

	x.escreve(out);
	return out;
}

