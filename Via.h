#ifndef Via_
#define Via_

#include "Local.h"
#include <string>
using namespace std;


class Via{

private:
	string desc;
	int comprimento;
	int tempo;
	Local* localInicio;
	Local* localFim;

public:
	Via();
	Via(Local* li, Local* lf, string d , int c, int t); //inicio, fim, desc, comprimento, tempo
	Via(string d , int c, int t); // desc, comprimento, tempo
	Via (const Via& v); //const copia
	           
	~Via();


	string getDesc();
	int getComprimento();
	virtual double getPreco() const;
	int getTempo();
	Local* getLocalInicio();
	Local* getLocalFim();

	void setDesc(const string& d);
	void setComprimento(const int& c);
	void setTempo(const int& t);
	void setLocalInicio( Local* li);
	void setLocalFim(Local* lf);

	bool operator == (const Via &v);
	bool operator <(const Via& v);
	const Via& operator = (const Via &v);

	virtual Via* clone();
	virtual void escreve(ostream&)const;

	
	 
}; //fim class -----------------------


Via::Via(){

	localInicio=new Local();
	localFim=new Local();
	(*this).desc="";
	(*this).comprimento=0;
	(*this).tempo=0;

}

	//cod, cumprim,tempo, inicio, fim
Via::Via(Local* li, Local* lf, string d, int c, int t){

	(*this).localInicio=li;
	(*this).localFim=lf;
	(*this).desc=d;
	(*this).comprimento=c;
	(*this).tempo=t;	
}
	

Via::Via(string d, int c, int t){

	(*this).localInicio=new Local();
	(*this).localFim=new Local();
	(*this).desc=d;
	(*this).comprimento=c;
	(*this).tempo=t;	
}
	
	
	//const copia
Via::Via (const Via& v){

	(*this).desc=v.desc;
	(*this).comprimento=v.comprimento;
	(*this).tempo=v.tempo;
	(*this).localInicio=v.localInicio;
	(*this).localFim=v.localFim;
}


	//destrut
Via::~Via(){
}


string Via::getDesc(){
	
	return desc;
}
	
int Via::getComprimento(){
	return comprimento;
}

double Via::getPreco() const {
	return 0;
}

int Via::getTempo(){

	return tempo;
}

Local* Via::getLocalInicio(){
	return localInicio;
}
	
Local* Via::getLocalFim(){

	return localFim;
}

void Via::setDesc(const string &d){
	(*this).desc=d;
}
	
void Via::setComprimento(const int &c){

	(*this).comprimento=c;

}

void Via::setTempo(const int &t){
	(*this).tempo=t;
}


void Via::setLocalInicio(Local* li){
	(*this).localInicio=li;
}

void Via::setLocalFim(Local* lf){
	(*this).localFim=lf;
}






void Via::escreve(ostream& out) const{

	out<<" Desc. Via: "<<desc <<endl
		<<" Local Inicio: " <<localInicio->getDesc()
		<<" Local Fim: "<<localFim->getDesc() <<endl
		<<" Comprimento via: "<<comprimento << "Km"
		<<" Tempo medio: "<<tempo <<"m";

}



Via* Via::clone(){

	return new Via(*this);
}

//----------------- operadores
// Criterio: Cumprimento


bool Via::operator <(const Via& v){

	return ( comprimento < v.comprimento);

}

bool Via::operator ==(const Via &v){
		if (desc == v.desc && localInicio == v.localInicio && localFim == v.localFim){
		return true;
	}else return false;
}

const Via& Via:: operator = (const Via& v){
		if(&v != this){
		(*this)=Via(v);
	}
	return *this;

}





//-----------OSTREAM

ostream& operator<<(ostream& out, const Via& x){

	x.escreve(out);
	return out;
}


#endif