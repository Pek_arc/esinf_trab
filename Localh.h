#ifndef Localh_
#define Localh_

#include <string>
using namespace std;

#include "Local.h"
#include "Utils.h"


class Localh : public Local{

	int tempo;
	int horaAbert;
	int horaFecho;


public:
	Localh();
	Localh(string d, int t, int a, int f);
	Localh(const Localh &l);
	virtual ~Localh();

	virtual int getTempo() const;
	virtual int getAbertura() const;
	virtual int getFecho() const;

	void setTempo(int t);
	void setAbetura(int a);
	void setFecho(int f);

	const Localh& operator = (const Localh &c);
	virtual bool operator==(const Localh &c);
	virtual void escreve (ostream& out) const;
	virtual Local * clone() const;

};



Localh::Localh(){
	}

Localh::Localh(string d, int t, int a, int f):Local(d){
	tempo=t;
	horaAbert=a;
	horaFecho=f;
}

Localh::Localh(const Localh &c):Local(c){

	tempo=c.tempo;
	horaAbert=c.horaAbert;
	horaFecho=c.horaFecho;
}



Localh::~Localh(){
}


int Localh::getTempo() const{
	return tempo;
}

int Localh::getAbertura() const{
	return horaAbert;
}

int Localh::getFecho() const{
	return horaFecho;
}


void Localh::setTempo(int s){
	tempo=s;
}

void Localh::setAbetura(int s){
	horaAbert=s;
}

void Localh::setFecho(int s){
	horaFecho=s;
}


const Localh &Localh:: operator = (const Localh &l){
		Local::operator=(l);
		tempo = l.tempo;
		horaAbert = l.horaAbert;
		horaFecho = l.horaFecho;

	return *this;

}

bool Localh::operator==(const Localh &l){
		if ((*this).getDesc() == l.getDesc() && tempo == l.tempo){
		return true;
	}else return false;
}

void Localh::escreve(ostream &out) const{
	Local::escreve(out);
	out << "Tempo Visita: " << tempo << "m"
		<<" Abertura: " << minToTime(horaAbert)
		<<" Fecho: " << minToTime(horaFecho) <<endl;
}

ostream& operator << (ostream& out, const Localh& l){
	l.escreve(out);
	return out;
}

Local * Localh::clone() const
{
	return new Localh (* this);
}



#endif