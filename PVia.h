#ifndef _PVia_
#define _PVia_

#include <ostream>

using namespace std;

#include "Via.h"
#include "AE.h"
#include "EN.h"

/*
Esta Classe foi criada para que os ramos do Grafo n�o contenham apontadores, mas, ao mesmo tempo, permite que o seu conteudo fosse
manipulado dinamicamente, quer atrav�s do polimorfismo (Via -> AE -> EN), quer atrav�s da manipula��o dinamica da variavel tipoComparacao
utilizada na sobrecarga de operadores.
*/
class PVia {
private:
	Via *ape;

	enum TipoComparacao { KMS , CUSTO , TEMPO}; 
	static TipoComparacao  tipoComparacao;
public:
	static void setComparacaoKMS();
	static void setComparacaoCUSTO();
	static void setComparacaoTEMPO();

	int getKM() const;
	double getPreco() const;
	int getTempo() const;
	string getDesc() const;
	

	PVia();
	PVia(Via* &e);
	PVia(string d, int c, int t ,double p);
	PVia(string d, int c, int t, string p);
	PVia(const PVia &e);
	~PVia();

	bool operator >(const PVia &e) const;
	bool operator <(const PVia &e) const;
	bool operator ==(const PVia &e) const;
	PVia operator+(const PVia &e);
	const PVia & operator+=(const PVia &e);
	const PVia & operator=(const PVia &e);
	void write(ostream &out) const;
};

PVia::TipoComparacao PVia::tipoComparacao=PVia::TipoComparacao::KMS;

void PVia::setComparacaoKMS() {
	tipoComparacao=TipoComparacao::KMS;
}
void PVia::setComparacaoCUSTO() {
	tipoComparacao=TipoComparacao::CUSTO;
}

void PVia::setComparacaoTEMPO() {
	tipoComparacao=TipoComparacao::TEMPO;
}

int PVia::getKM() const {
	return ape->getComprimento();
}

string PVia::getDesc() const {
	return ape->getDesc();
}

double PVia::getPreco() const {
	return ape->getPreco();
}

int PVia::getTempo() const {
	return ape->getTempo();
}
PVia::PVia() {
	this->ape = new AE();
}
PVia::PVia(string d, int c, int t ,double p) {
	ape = new AE( d,  c, t , p);
}


PVia::PVia(string d, int c, int t ,string p) {
	ape = new EN( d,  c, t , p);
}


PVia::PVia(Via* &e) {
	this->ape = e->clone();
}
PVia::PVia(const PVia &e) {
	this->ape = e.ape->clone();
}
PVia::~PVia() {
	delete ape;
}


bool PVia::operator >(const PVia &e) const {	
	if (tipoComparacao==TipoComparacao::KMS) return (*this).getKM() > e.getKM();
	if (tipoComparacao==TipoComparacao::CUSTO){
		if((*this).getPreco() == e.getPreco()){
			return (*this).getKM() > e.getKM();
		}
		return (*this).getPreco() > e.getPreco();
	}
	return (*this).getTempo() > e.getTempo();
}

bool PVia::operator <(const PVia &e) const {
	if (tipoComparacao==TipoComparacao::KMS) return (*this).getKM() < e.getKM();
	if (tipoComparacao==TipoComparacao::CUSTO){
		if((*this).getPreco() == e.getPreco()){
			return (*this).getKM() < e.getKM();
		}
		return (*this).getPreco() < e.getPreco();
	}
	return (*this).getTempo() < e.getTempo();
}
bool PVia::operator ==(const PVia &e) const {
	if (tipoComparacao==TipoComparacao::KMS) return (*this).getKM() == e.getKM();
	if (tipoComparacao==TipoComparacao::CUSTO) return (*this).getPreco() == e.getPreco();
	return (*this).getTempo() == e.getTempo();
}
PVia PVia::operator+(const PVia &e) {
	return PVia("", (*this).getKM()+e.getKM(), (*this).getTempo()+e.getTempo(), (*this).getPreco()+e.getPreco());
}
const PVia & PVia::operator+=(const PVia &e) {
	this->ape->setComprimento(this->ape->getComprimento()+e.ape->getComprimento());
		this->ape->setTempo(this->ape->getTempo()+e.ape->getTempo());
	if (typeid(*ape)==typeid(AE)) {
		AE *ae = (AE *)this->ape;
		ae->setPreco(ae->getPreco()+e.ape->getPreco());
	}	
	return  *this;
}
const PVia & PVia::operator=(const PVia &e) {
	this->ape = e.ape->clone();
	return *this;
}
void PVia::write(ostream &out) const {
	out << *ape;
}


ostream &operator <<(ostream &out, const PVia &e)
{
	e.write(out);
	return out;
}


#endif