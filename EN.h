#ifndef EN_
#define EN_

#include <string>
using namespace std;

#include "Via.h"
#include "Local.h"

class EN : public Via{

private:
string pav;

public:
	EN();
	// inicio, fim, cod,  comp, tempo, pavimento
	EN(Local* &li, Local* &lf, string d, int c, int t, string p);
	EN(string d, int c, int t, string p);
	EN(const EN&);
	~EN();

	string getpav();
	void setpav(string);

	virtual double getPreco() const; //Embora a variavel pre�o nao exista, serve para poder comparar o custo de uma AE e uma EN
	virtual Via * clone();
	const EN& operator = (const EN &v);
	virtual void escreve(ostream& out)const;


};// fim classe -----------------------------

EN::EN():Via(){

	pav="sem pav";
}

EN::EN( Local * &li,  Local * &lf, string d, int c, int t, string p):
	Via(li, lf, d,c,t){
		(*this).setpav(p);
}

EN::EN( string d, int c, int t, string p):
	Via(d,c,t){
	(*this).setpav(p);
}

EN::EN(const EN& e)
	  :Via(e){

		  (*this).setpav(e.pav);
}


EN::~EN(){}

string EN::getpav(){

	return (*this).pav;

}

double EN::getPreco() const{

	return 0;

}

void EN::setpav(string p){

	(*this).pav=p;
}

const EN& EN:: operator = (const EN& n){
		if(&n != this){
		(*this)=EN(n);
	}
	return *this;

}

void EN::escreve(ostream& out)const{

	Via::escreve(out);
	out<<" Pav: "<<pav<<endl;
}

Via * EN::clone(){

	return new EN(*this);
}


//------------OSTREAM

ostream& operator<<(ostream& out, EN& x){

	x.escreve(out);
	return out;
}

#endif

