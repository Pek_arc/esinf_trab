#ifndef Teste_
#define Teste_

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <list>
#include <queue>
#include "Local.h"
#include "Localn.h"
#include "Localh.h"
#include "Via.h"
#include "AE.h"
#include "EN.h"
#include "Mapa.h"

using namespace std;


class Teste{

private:
	list <Local*> vecLocais;
	list <Via*> vecVias;



public:
	Teste();
	Teste(const Teste&t);
	~Teste();

	void setVecLocais(const list <Local*>);
	void setVecVias(const list <Via*>);
	list <Local*> getVecLocais();
	list <Via*> getVecVias();

	void inserirLocais(Local *l);
	void inserirVias(Via *v);
	void listarLocais();
	void listarVias();
	int lerFicheiroLocais(string f);
	int lerFicheiroVias(string fv);
	bool find(Local* &l, string);
	bool exist(string);
	bool validateFloat(string);
	static bool comparaLocais(Local * f, Local * second);
	void contabLocais();
	void ordenarLocais();
	void constroiGrafo(Mapa &m);


};

Teste::Teste()
{
}


Teste::Teste(const Teste &t)
{
	(*this).vecLocais.assign(t.vecLocais.begin(), t.vecLocais.end());
	(*this).vecVias.assign(t.vecVias.begin(), t.vecVias.end());
}

Teste::~Teste()
{

	vecLocais.clear();
	vecVias.clear();
}

list <Local*> Teste::getVecLocais(){
	return (*this).vecLocais;
}


void Teste::inserirLocais(Local *l)
{
	vecLocais.push_back(l);
}

void Teste::inserirVias(Via *v)
{
	vecVias.push_back(v);
};

void Teste::constroiGrafo(Mapa &m)
{

	for (list<Local*>::iterator it = vecLocais.begin(); it != vecLocais.end(); it++){
		m.addGraphVertex(*it);
	}


	for (list<Via*>::iterator it = vecVias.begin(); it != vecVias.end(); it++){
		m.addGraphEdge(PVia(*it), (*it)->getLocalInicio(), (*it)->getLocalFim());
	}

}



void Teste::listarLocais()
{
	for (list<Local*>::iterator it = vecLocais.begin(); it != vecLocais.end(); it++){
		cout << *(*it) << endl;
	}

}

void Teste::listarVias()
{
	for (list<Via*>::iterator it = vecVias.begin(); it != vecVias.end(); it++)
		cout << (*(*it)) << endl;

}

bool Teste::find(Local* &l, string s){
	for (list<Local*>::iterator it = vecLocais.begin(); it != vecLocais.end(); it++){

		if ((*it)->getDesc() == s){
			l = (*it)->clone();
			return true;
		}
	}
	return false;
}

bool Teste::exist(string s){
	for (list<Local*>::iterator it = vecLocais.begin(); it != vecLocais.end(); it++){

		if ((*it)->getDesc() == s)
			return true;
	}
	return false;
}


bool Teste::validateFloat(string input) {
	bool valid = true;
	try{
		float num = std::stof(input);
	}
	catch (const std::exception e){
		valid = false;
	}
	return valid;
}

void Teste::ordenarLocais(){
	vecLocais.sort(&Teste::comparaLocais);
}

bool Teste::comparaLocais(Local* first, Local* second){
	return(*first < *second);
}

void Teste::contabLocais(){
	queue <Local*> n;
	queue <Local*> h;
	for (list<Local*>::iterator it = vecLocais.begin(); it != vecLocais.end(); it++){
		if (typeid(Localn) == typeid(**it)){
			n.push(*it);
		}
		else if (typeid(Localh) == typeid(**it)){
			h.push(*it);
		}
	}
	cout << endl << "Locais Naturais: " << n.size() << endl;
	while (!n.empty()) {
		cout << *n.front() << endl;
		n.pop();
	}
	cout << endl << "Locais historicos: " << h.size() << endl;
	while (!h.empty()) {
		cout << *h.front() << endl;
		h.pop();
	}
}


int Teste::lerFicheiroLocais(string f){
	string linha;
	Local * l;
	int inic = 0;
	ifstream fx;
	string argL[4];
	fx.open(f);
	if (!fx)
	{
		cout << "Fx. nao existe !" << endl;
		return -1;
	}
	while (!fx.eof()){
		int arg = 0;
		int pos = 0;
		getline(fx, linha, '\n');
		if (linha.size() > 0)
		{
			int pos = linha.find(',', inic);
			while (pos > 0){
				pos = linha.find(',', inic);
				argL[arg] = (linha.substr(inic, pos - inic));
				pos++;
				inic = pos;
				arg++;
			}
			if (arg == 2 || arg == 4){
				if (!exist(argL[0])){
					if (arg == 2){
						l = new Localn(argL[0], stof(argL[1]));
						(*this).inserirLocais(l);
					}
					else{
						l = new Localh(argL[0], stoi(argL[1]), stoi(argL[2]), stoi(argL[3]));
						(*this).inserirLocais(l);
					}
				}
				else cout << argL[0] << " ja existente" << endl;
			}
			else cout << "Erro ao Ler uma linha" << endl;
		}
	}
	fx.close();
	return 0;
}

int Teste::lerFicheiroVias(string fv){
	string linha;
	Via * v;
	int inic = 0;
	ifstream fx;
	string argL[6];
	fx.open(fv);
	if (!fx)
	{
		cout << "Fx. nao existe !" << endl;
		return -1;
	}
	while (!fx.eof()){
		int arg = 0;
		int pos = 0;
		getline(fx, linha, '\n');
		if (linha.size() > 0)
		{
			int pos = linha.find(',', inic);
			while (pos > 0){
				pos = linha.find(',', inic);
				argL[arg] = (linha.substr(inic, pos - inic));
				pos++;
				inic = pos;
				arg++;
			}
			if (arg == 6){
				Local* lOrigin = new Local();
				Local* lDest = new Local();
				if (!find(lOrigin, argL[0]) || !find(lDest, argL[1])){
					cout << "Um dos Locais nao existe, Via: " << argL[2] << " nao criada" << endl;
				}
				else {
					if (validateFloat(argL[5])){
						v = new AE(lOrigin, lDest, argL[2], stoi(argL[3]), stoi(argL[4]), stof(argL[5]));
						(*this).inserirVias(v);
					}
					else{
						v = new EN(lOrigin, lDest, argL[2], stoi(argL[3]), stoi(argL[4]), argL[5]);
						(*this).inserirVias(v);
					}
				}
			}
			else cout << "Erro ao Ler uma linha" << endl;
		}
	}
	fx.close();
	return 0;
}


#endif